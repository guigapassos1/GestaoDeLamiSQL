package br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiBO;

import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiDOMAIN.ReservaDOMAIN;
import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiTUI.ConexaoSQL;

public class ProfessorBO {

	static Scanner sc = new Scanner(System.in);
	static ConexaoSQL con = new ConexaoSQL();

	public void Professor(ReservaDOMAIN reservaDOMAIN) {

		Boolean valorBooleano = true;
		int escolha = 0;
		while (valorBooleano) {
			System.out.println("\nDeseja visualizar lista de salas LIVRES? (1)");
			System.out.println("Deseja visualizar lista de salas OCUPADAS? (2)");
			System.out.println("Deseja reservar alguma sala? (3)");
			System.out.println("Deseja fazer trocar de usu�rio? (4)\n");
			escolha = sc.nextInt();
			if (escolha == 1) {
				AdministradorBO.listarSalas();
			} else if (escolha == 2) {
				listarSalasReservadas();
			} else if (escolha == 3) {
				criarReserva(reservaDOMAIN);
				adicionarReserva(reservaDOMAIN);
			} else if (escolha == 4) {
				LoginBO.usuario();
			}
		}
	}

	public static void criarReserva(ReservaDOMAIN reservaDOMAIN) {
		System.out.println("-----RESERVAR SALA-----");
		System.out.println("Id da sala:");
		String idDaSala = sc.next();
		String sql = "DELETE FROM salas" + " WHERE id = '" + idDaSala + "'";
		con.enviarSQL(sql);
		System.out.println("Nome da sala:");
		reservaDOMAIN.setNomeDaSala(sc.next());
		System.out.println("Digite o Id da reserva:");
		reservaDOMAIN.setId(sc.nextInt());
		System.out.println("Digite o nome do professor:");
		reservaDOMAIN.setNomeDoProfessor(sc.next());
		converterStringDate(reservaDOMAIN);
	}
	private static void converterStringDate(ReservaDOMAIN reservaDOMAIN) {
		System.out.println("Informe a data da reserva (dd/mm/aaaa):");
		String dataString = sc.next();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date data = sdf.parse(dataString.toString());
			String dataFormatada =  sdf.format(data);
			reservaDOMAIN.setDia(dataFormatada);
		} catch (ParseException e) {
			System.out.println("Data invalida!");
		}
	}


	public static void adicionarReserva(ReservaDOMAIN reservaDOMAIN) {
		String sql = "INSERT into reserva (id, nomedoprofessor, nomedasala, dia, iddasala)" + "values ('"
				+ reservaDOMAIN.getId() + "', '" + reservaDOMAIN.getNomeDoProfessor() + "', '"
				+ reservaDOMAIN.getNomeDaSala() + "', '" + reservaDOMAIN.getDia() + "', '" + 1 + "')";
		int res = con.enviarSQL(sql);
		if (res > 0) {
			System.out.println("\nReserva realizada com sucesso\n");
		} else {
			System.out.println("###     ERRO     ###");
			System.out.println("### ID DUPLICADA ###");
		}
	}

	public static void listarSalasReservadas() {
		String sql = "Select * from reserva";
		ResultSet rs = con.buscarSQL(sql);
		try {
			System.out.println("\n###LISTA DE SALAS OCUPADAS###\n");
			while (rs.next()) {
				int id = rs.getInt("id");
				String nomedoprofessor = rs.getString("nomedoprofessor");
				String nomedasala = rs.getString("nomedasala");
				String dia = rs.getString("dia");
				System.out.println("Id: " + id + "\nNome do Professor: " + nomedoprofessor + "\nNome da Sala: "
						+ nomedasala + "\nDia da Reserva: " + dia + "\n--------------------\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}