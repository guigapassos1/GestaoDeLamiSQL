package br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiBO;

import java.util.Scanner;

import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiDOMAIN.ReclamacaoDOMAIN;
import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiTUI.ConexaoSQL;

public class VisitanteBO {

	static Scanner sc = new Scanner(System.in);
	static ConexaoSQL con = new ConexaoSQL();

	public void Visitante(ReclamacaoDOMAIN pecaDaSalaDOMAIN) {

		Boolean valorBooleano = true;
		int escolha = 0;
		while (valorBooleano) {
			System.out.println("\nDeseja visualizar lista de salas LIVRES? (1)");
			System.out.println("Deseja visualizar lista de salas OCUPADAS? (2)");
			System.out.println("Deseja comentar algo em alguma sala? (3)");
			System.out.println("Deseja fazer trocar de usu�rio? (4)\n");
			escolha = sc.nextInt();
			if (escolha == 1) {
				AdministradorBO.listarSalas();
			} else if (escolha == 2) {
				ProfessorBO.listarSalasReservadas();
			}else if (escolha == 3) {
				ReclamacaoBO.criarComentario(pecaDaSalaDOMAIN);
				ReclamacaoBO.adicionarComentario(pecaDaSalaDOMAIN);
			} else if (escolha == 4) {
				LoginBO.trocarUsuarios();
			}
		}
	}

}