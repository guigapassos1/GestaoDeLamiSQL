package br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiBO;

import java.sql.ResultSet;
import java.util.Scanner;

import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiDOMAIN.ReclamacaoDOMAIN;
import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiTUI.ConexaoSQL;

public class ReclamacaoBO {

	static Scanner sc = new Scanner(System.in);
	static ConexaoSQL con = new ConexaoSQL();

	public static void criarComentario(ReclamacaoDOMAIN pecaDaSalaDOMAIN) {
		System.out.println("-----COMENTARIO-----");
		System.out.println("Id da sala:");
		pecaDaSalaDOMAIN.setIdDaSala(sc.nextInt());
		System.out.println("Id do objeto:");
		pecaDaSalaDOMAIN.setId(sc.nextInt());
		System.out.println("Nome do aluno:");
		pecaDaSalaDOMAIN.setNomeDoAluno(sc.next());
		System.out.println("Nome do objeto:");
		pecaDaSalaDOMAIN.setPecaComDefeito(sc.next());
		System.out.println("Problema do objeto:");
		pecaDaSalaDOMAIN.setObsSobrePeca(sc.next());
		pecaDaSalaDOMAIN.setStatus("pendente");
		pecaDaSalaDOMAIN.setNomeDoTecnico("nenhum");
	}

	public static void adicionarComentario(ReclamacaoDOMAIN pecaDaSalaDOMAIN) {
		String sql = "INSERT into reclamacao (iddasala, id, nomedoaluno, nome, problema, status, nomedotecnico)"
				+ "values ('" + pecaDaSalaDOMAIN.getIdDaSala() + "', '" + pecaDaSalaDOMAIN.getId() + "', '"
				+ pecaDaSalaDOMAIN.getNomeDoAluno() + "', '" + pecaDaSalaDOMAIN.getPecaComDefeito() + "', '"
				+ pecaDaSalaDOMAIN.getObsSobrePeca() + "', '" + pecaDaSalaDOMAIN.getStatus().toUpperCase() + "', '"
				+ pecaDaSalaDOMAIN.getNomeDoTecnico().toUpperCase() + "')";
		con.enviarSQL(sql);

	}

	public void listarReclamacao() {
		String sql = "Select * from reclamacao";
		ResultSet rs = con.buscarSQL(sql);
		try {
			System.out.println("###Lista de Reclamações###\n");
			while (rs.next()) {
				int iddasala = rs.getInt("iddasala");
				int id = rs.getInt("id");
				String nomedoaluno = rs.getString("nomedoaluno");
				String nome = rs.getString("nome");
				String problema = rs.getString("problema");
				String status = rs.getString("status");
				String nomedotecnico = rs.getString("nomedotecnico");
				System.out.println("Nome do Aluno: " + nomedoaluno  + "\nId da Sala: " +iddasala  + "\nId do Objeto: "
						+ id + "\nNome: " + nome + "\nProblema: " + problema + "\nStatus: " + status
						+ "\nTecnico: " + nomedotecnico + "\n--------------------");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}