package br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiBO;

import java.util.Scanner;

import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiDOMAIN.ReclamacaoDOMAIN;
import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiDOMAIN.SalaDOMAIN;
import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiTUI.ConexaoSQL;

public class TecnicoBO {

	static Scanner sc = new Scanner(System.in);
	static ConexaoSQL con = new ConexaoSQL();
	static ReclamacaoDOMAIN reclamacaoDOMAIN = new ReclamacaoDOMAIN();
	static ReclamacaoBO reclamacaoBO = new ReclamacaoBO();

	public void Tecnico() {

		Boolean valorBooleano = true;
		int escolha = 0;
		while (valorBooleano) {
			System.out.println("\nDeseja visualizar lista de salas LIVRES? (1)");
			System.out.println("Deseja visualizar lista de salas OCUPADAS? (2)");
			System.out.println("Deseja visualizar reclamações? (3)");
			System.out.println("Deseja atualizar reclamações? (4)");
			System.out.println("Deseja fazer trocar de usuário? (5)\n");
			escolha = sc.nextInt();
			if (escolha == 1) {
				AdministradorBO.listarSalas();
			} else if (escolha == 2) {
				ProfessorBO.listarSalasReservadas();
			}else if (escolha == 3) {
				reclamacaoBO.listarReclamacao();
			} else if (escolha == 4) {
				atualizarReclamação(reclamacaoDOMAIN);
			} else if (escolha == 5) {
				LoginBO.usuario();
			}
		}
	}

	public static void atualizarReclamação(ReclamacaoDOMAIN reclamacaoDOMAIN) {
		System.out.println("-----ATUALIZAR RECLAMAÇÃO-----");
		System.out.println("Digite o Id da sala:");
		int idDaSala = sc.nextInt();
		System.out.println("Digite o Id da reclamação:");
		int idDaReclamação = sc.nextInt();
		System.out.println("Digite o nome do tecnico:");
		reclamacaoDOMAIN.setNomeDoTecnico(sc.next());
		String sql = "UPDATE reclamacao" + " SET nomedotecnico = '" + reclamacaoDOMAIN.getNomeDoTecnico() + "'"
				+ " WHERE id = '" + idDaReclamação + "'";
		con.enviarSQL(sql);
		reclamacaoDOMAIN.setStatus("CONCLUIDO");
		sql = "UPDATE reclamacao" + " SET status = '" + reclamacaoDOMAIN.getStatus() + "'" + " WHERE id = '"
				+ idDaReclamação + "'";
		con.enviarSQL(sql);

	}
}