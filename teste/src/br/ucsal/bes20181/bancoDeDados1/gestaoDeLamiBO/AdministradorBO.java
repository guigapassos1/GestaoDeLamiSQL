package br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiBO;

import java.sql.ResultSet;
import java.util.Scanner;

import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiDOMAIN.SalaDOMAIN;
import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiDOMAIN.UsuarioDOMAIN;
import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiTUI.ConexaoSQL;

public class AdministradorBO {

	static UsuarioDOMAIN usuarioDOMAIN = new UsuarioDOMAIN();
	static ReclamacaoBO PecaDaSalaBO = new ReclamacaoBO();
	static UsuarioBO usuarioBO = new UsuarioBO();
	static TecnicoBO tecnicoBO = new TecnicoBO();
	static Scanner sc = new Scanner(System.in);
	static ConexaoSQL con = new ConexaoSQL();

	public void Admistrador(SalaDOMAIN salaDOMAIN) {
		Boolean valorBooleano = true;
		int escolha = 0;

		while (valorBooleano) {
			System.out.println("\nDeseja criar sala? (1)");
			System.out.println("Deseja remover sala? (2)");
			System.out.println("Deseja editar sala? (3)");
			System.out.println("Deseja editar usu�rios? (4)");
			System.out.println("Deseja visualizar lista de salas LIVRES? (5)");
			System.out.println("Deseja visualizar lista de salas OCUPADAS? (6)");
			System.out.println("Deseja fazer trocar de usu�rio? (7)\n");
			escolha = sc.nextInt();
			if (escolha == 1) {
				criarSala(salaDOMAIN);
				adicionarSala(salaDOMAIN);
			} else if (escolha == 2) {
				removerSala(salaDOMAIN);
			} else if (escolha == 3) {
				atualizarSala(salaDOMAIN);
			} else if (escolha == 4) {
				usuarioBO.editarUsuarios(usuarioDOMAIN);
			} else if (escolha == 5) {
				listarSalas();
			} else if (escolha == 6) {
				ProfessorBO.listarSalasReservadas();
			}else if (escolha == 7) {
				LoginBO.usuario();
			}
		}
	}

	public static void criarSala(SalaDOMAIN salaDOMAIN) {
		System.out.println("-----CRIAR SALA-----");
		System.out.println("Id da sala:");
		salaDOMAIN.setId(sc.nextInt());
		System.out.println("Nome da sala:");
		salaDOMAIN.setNome(sc.next());
		System.out.println("Capacidade total de pessoas:");
		salaDOMAIN.setCapacidadeDePessoas(sc.nextInt());
		System.out.println("Quantidade total de computadores:");
		salaDOMAIN.setQuantidadeTotalPcs(sc.nextInt());
		System.out.println("Quantidade de computadores funcionando:");
		salaDOMAIN.setPcsFuncionando(sc.nextInt());
	}

	public static void adicionarSala(SalaDOMAIN salaDOMAIN) {
		String sql = "INSERT into salas (id, nome,capacidadedepessoas, quantidadetotalpcs, pcsfuncionando)"
				+ "values ('" + salaDOMAIN.getId() + "', '" + salaDOMAIN.getNome() + "', '"
				+ salaDOMAIN.getCapacidadeDePessoas() + "', '" + salaDOMAIN.getQuantidadeTotalPcs() + "', '"
				+ salaDOMAIN.getPcsFuncionando() + "')";
		int res = con.enviarSQL(sql);
		if (res > 0) {
			System.out.println("\nCadastro realizado com sucesso");
		} else {
			System.out.println("###     ERRO     ###");
			System.out.println("### ID DUPLICADA ###");
		}
	}

	public static void removerSala(SalaDOMAIN salaDOMAIN) {
		System.out.println("-----REMOVER SALA-----");
		System.out.println("Id da sala:");
		String idDaSala = sc.next();
		String sql = "DELETE FROM salas" + " WHERE id = '" + idDaSala + "'";
		con.enviarSQL(sql);
	}

	public static void atualizarSala(SalaDOMAIN salaDOMAIN) {
		System.out.println("-----ATUALIZAR SALA-----");
		int escolha;
		System.out.println("Digite o Id da sala:");
		int idDaSala = sc.nextInt();
		System.out.println("\nDeseja editar nome? (1)");
		System.out.println("Deseja editar capacidade de pessoas? (2)");
		System.out.println("Deseja editar quantidade total de computadores? (3)\n");
		escolha = sc.nextInt();
		if (escolha == 1) {
			System.out.println("Novo nome:");
			salaDOMAIN.setNome(sc.next());
			String sql = "UPDATE salas" + " SET nome = '" + salaDOMAIN.getNome() + "'" + " WHERE id = '" + idDaSala
					+ "'";
			con.enviarSQL(sql);
		} else if (escolha == 2) {
			System.out.println("Nova capacidade de pessoas:");
			salaDOMAIN.setCapacidadeDePessoas(sc.nextInt());
			String sql = "UPDATE salas" + " SET capacidadedepessoas = '" + salaDOMAIN.getCapacidadeDePessoas() + "'"
					+ " WHERE id = '" + idDaSala + "'";
			con.enviarSQL(sql);
		} else if (escolha == 3) {
			System.out.println("Nova quantidade total de computadores:");
			salaDOMAIN.setQuantidadeTotalPcs(sc.nextInt());
			String sql = "UPDATE salas" + " SET quantidadetotalpcs = '" + salaDOMAIN.getQuantidadeTotalPcs() + "'"
					+ " WHERE id = '" + idDaSala + "'";
			con.enviarSQL(sql);
		}
	}

	public static void listarSalas() {
		String sql = "Select * from salas";
		ResultSet rs = con.buscarSQL(sql);
		try {
			System.out.println("\n###LISTA DE SALAS LIVRES###\n");
			while (rs.next()) {
				int id = rs.getInt("id");
				String nome = rs.getString("nome");
				String capacidadedepessoas = rs.getString("capacidadedepessoas");
				String quantidadetotalpcs = rs.getString("quantidadetotalpcs");
				String pcsfuncionando = rs.getString("pcsfuncionando");
				System.out.println("--------------------\nId: " + id + "\nNome: " + nome
						+ "\nCapacidade total de pessoas: " + capacidadedepessoas + "\nQuantidade total de pcs: "
						+ quantidadetotalpcs + "\nPcs Funcionando: " + pcsfuncionando+"\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}