package br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiBO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiDOMAIN.ReclamacaoDOMAIN;
import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiDOMAIN.ReservaDOMAIN;
import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiDOMAIN.SalaDOMAIN;
import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiDOMAIN.UsuarioDOMAIN;
import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiTUI.ConexaoSQL;

public class LoginBO {
	static AdministradorBO administradorBO = new AdministradorBO();
	static SalaDOMAIN salaDOMAIN = new SalaDOMAIN();
	static ReclamacaoDOMAIN pecaDaSalaDOMAIN = new ReclamacaoDOMAIN();
	static ReclamacaoBO pecaDaSalaBO = new ReclamacaoBO();
	static UsuarioDOMAIN usuarioDOMAIN = new UsuarioDOMAIN();
	static UsuarioBO usuarioBO = new UsuarioBO();
	static TecnicoBO tecnicoBO = new TecnicoBO();
	static VisitanteBO alunoBO = new VisitanteBO();
	static ProfessorBO professorBO = new ProfessorBO();
	static ReservaDOMAIN reservaDOMAIN = new ReservaDOMAIN();
	static Scanner sc = new Scanner(System.in);
	static ConexaoSQL con = new ConexaoSQL();
	static String sql = "";
	static String login = "";
	static String senha = "";

	public static void usuario() {

		Boolean valorBooleano = true;
		int escolha = 0;
		while (valorBooleano) {
			System.out.println("\n-------Bem vindo ao aplicativo de Gest�o de Lamis-------\n");
			System.out.println("\nDeseja entrar com uma conta? (1)");
			System.out.println("Deseja entrar como visitante? (2)\n");
			escolha = sc.nextInt();
			if (escolha == 1) {
				trocarUsuarios();
			} else if (escolha == 2) {
				alunoBO.Visitante(pecaDaSalaDOMAIN);
			}
		}
	}

	public static void trocarUsuarios() {
		System.out.println("--LOGAR--");
		System.out.println("Usu�rio:");
		login = sc.next();
		System.out.println("Senha:");
		senha = sc.next();
		sql = "Select * from administrador WHERE login = '" + login + "'";

		try {
			con.executaSQL(sql);
			con.rs.first();
			if (con.rs.getString("senha").equals(senha)) {
				System.out.println("\nLogin realizado como Administrador");
				administradorBO.Admistrador(salaDOMAIN);
			} else {
				System.out.println("Senha errada");
			}

		} catch (Exception e) {
			logarTecnico();
		}
	}

	private static void logarTecnico() {
		sql = "Select * from tecnico WHERE login = '" + login + "'";

		try {
			con.executaSQL(sql);
			con.rs.first();
			if (con.rs.getString("senha").equals(senha)) {
				System.out.println("\nLogin realizado como Tecnico\n");
				tecnicoBO.Tecnico();
			} else {
				System.out.println("Senha errada");
			}

		} catch (Exception e) {
			logarProfessor();
		}
	}

	private static void logarProfessor() {
		sql = "Select * from professor WHERE login = '" + login + "'";

		try {
			con.executaSQL(sql);
			con.rs.first();
			if (con.rs.getString("senha").equals(senha)) {
				System.out.println("\nLogin realizado como Professor\n");
				professorBO.Professor(reservaDOMAIN);
			} else {
				System.out.println("Senha errada");
			}

		} catch (Exception e) {
			System.out.println("Login errado");
		}
	}
}
