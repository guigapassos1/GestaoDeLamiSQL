package br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiBO;

import java.sql.ResultSet;
import java.util.Scanner;

import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiDOMAIN.SalaDOMAIN;
import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiDOMAIN.UsuarioDOMAIN;
import br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiTUI.ConexaoSQL;

public class UsuarioBO {
	Scanner sc = new Scanner(System.in);
	static ConexaoSQL con = new ConexaoSQL();
	SalaDOMAIN salaDOMAIN = new SalaDOMAIN();
	AdministradorBO administradorBO = new AdministradorBO();

	public void editarUsuarios(UsuarioDOMAIN usuarioDOMAIN) {
		Boolean valorBooleano = true;
		int escolha = 0;
		while (valorBooleano) {
			System.out.println("\nDeseja criar usu�rio? (1)");
			System.out.println("Deseja remover usu�rio? (2)");
			System.out.println("Deseja editar usu�rio? (3)");
			System.out.println("Deseja ver lista de usu�rios? (4)");
			System.out.println("Deseja voltar? (5)\n");
			escolha = sc.nextInt();
			if (escolha == 1) {
				criarUsuario(usuarioDOMAIN);
				adicionarUsuario(usuarioDOMAIN);
			} else if (escolha == 2) {
				removerUsuario(usuarioDOMAIN);
			} else if (escolha == 3) {
				atualizarUsuario(usuarioDOMAIN);
			} else if (escolha == 4) {
				listarUsuarios();
			} else if (escolha == 5) {
				administradorBO.Admistrador(salaDOMAIN);
			}
		}
	}

	public void criarUsuario(UsuarioDOMAIN usuarioDOMAIN) {
		System.out.println("-----CRIAR USUARIO-----");
		System.out.println("ID:");
		usuarioDOMAIN.setId(sc.nextInt());
		System.out.println("Tipo:");
		usuarioDOMAIN.setTipo(sc.next().toUpperCase());
		System.out.println("Nome:");
		usuarioDOMAIN.setNome(sc.next());
		System.out.println("Sobrenome:");
		usuarioDOMAIN.setSobrenome(sc.next());
		System.out.println("Email:");
		usuarioDOMAIN.setEmail(sc.next());
		System.out.println("Login:");
		usuarioDOMAIN.setLogin(sc.next());
		System.out.println("Senha:");
		usuarioDOMAIN.setSenha(sc.next());
	}

	public void adicionarUsuario(UsuarioDOMAIN usuarioDOMAIN) {
		int res;
		String sql = "";
		if (usuarioDOMAIN.getTipo().equals("PROFESSOR")) {
			sql = "INSERT into professor (id, login, senha, tipo, nome, sobrenome, email)" + "values ('"
					+ usuarioDOMAIN.getId() + "', '" + usuarioDOMAIN.getLogin() + "', '" + usuarioDOMAIN.getSenha()
					+ "', '" + usuarioDOMAIN.getTipo() + "', '" + usuarioDOMAIN.getNome() + "', '"
					+ usuarioDOMAIN.getSobrenome() + "', '" + usuarioDOMAIN.getEmail() + "')";
		} else if (usuarioDOMAIN.getTipo().equals("ADMINISTRADOR")) {
			sql = "INSERT into administrador (id, login, senha, tipo, nome, sobrenome, email)" + "values ('"
					+ usuarioDOMAIN.getId() + "', '" + usuarioDOMAIN.getLogin() + "', '" + usuarioDOMAIN.getSenha()
					+ "', '" + usuarioDOMAIN.getTipo() + "', '" + usuarioDOMAIN.getNome() + "', '"
					+ usuarioDOMAIN.getSobrenome() + "', '" + usuarioDOMAIN.getEmail() + "')";
		} else if (usuarioDOMAIN.getTipo().equals("TECNICO")) {
			sql = "INSERT into tecnico (id, login, senha, tipo, nome, sobrenome, email)" + "values ('"
					+ usuarioDOMAIN.getId() + "', '" + usuarioDOMAIN.getLogin() + "', '" + usuarioDOMAIN.getSenha()
					+ "', '" + usuarioDOMAIN.getTipo() + "', '" + usuarioDOMAIN.getNome() + "', '"
					+ usuarioDOMAIN.getSobrenome() + "', '" + usuarioDOMAIN.getEmail() + "')";
		}
		res = con.enviarSQL(sql);
		if (res > 0) {
			System.out.println("Usu�rio Criado");
		} else {
			System.out.println("###     ERRO     ###");
		}
	}

	public void removerUsuario(UsuarioDOMAIN usuarioDOMAIN) {
		System.out.println("-----REMOVER USUARIO-----");
		System.out.println("Usu�rio:");
		String usuario = sc.next();
		String sql = "DELETE FROM administrador" + " WHERE login = '" + usuario + "'";
		con.enviarSQL(sql);
	}

	public void atualizarUsuario(UsuarioDOMAIN usuarioDOMAIN) {
		System.out.println("-----ATUALIZAR USUARIO-----");
		int escolha;
		System.out.println("Digite o nome do usuario:");
		String usuario = sc.next();
		System.out.println("\nDeseja editar nome? (1)");
		System.out.println("Deseja editar senha? (2)");
		System.out.println("Deseja editar tipo? (3)\n");
		escolha = sc.nextInt();
		if (escolha == 1) {
			System.out.println("Novo nome:");
			usuarioDOMAIN.setLogin(sc.next());
			String sql = "UPDATE usuario" + " SET login = '" + usuarioDOMAIN.getLogin() + "'" + " WHERE login = '"
					+ usuario + "'";
			con.enviarSQL(sql);
		} else if (escolha == 2) {
			System.out.println("Nova senha:");
			usuarioDOMAIN.setSenha(sc.next());
			String sql = "UPDATE usuario" + " SET senha = '" + usuarioDOMAIN.getSenha() + "'" + " WHERE login = '"
					+ usuario + "'";
			con.enviarSQL(sql);
		} else if (escolha == 3) {
			System.out.println("Nova tipo:");
			usuarioDOMAIN.setTipo(sc.next());
			String sql = "UPDATE usuario" + " SET tipo = '" + usuarioDOMAIN.getTipo() + "'" + " WHERE login = '"
					+ usuario + "'";
			con.enviarSQL(sql);
		}
	}

	public void listarUsuarios() {
		listarAluno();
		listarAdministrador();
		listarProfessor();
		listarTecnico();
	}

	private void listarTecnico() {
		String sql;
		ResultSet rs;
		sql = "Select * from tecnico";
		rs = con.buscarSQL(sql);
		try {
			System.out.println("\n###LISTA DE TECNICOS###\n");
			while (rs.next()) {
				int id = rs.getInt("id");
				String login = rs.getString("login");
				String senha = rs.getString("senha");
				String nome = rs.getString("nome");
				String sobrenome = rs.getString("sobrenome");
				String email = rs.getString("email");
				System.out.println("--------------------\nId: " + id + "\nLogin: " + login + "\nSenha: " + senha
						+ "\nNome: " + nome + "\nSobrenome: " + sobrenome + "\nEmail: " + email + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void listarProfessor() {
		String sql;
		ResultSet rs;
		sql = "Select * from professor";
		rs = con.buscarSQL(sql);
		try {
			System.out.println("\n###LISTA DE PROFESSORES###\n");
			while (rs.next()) {
				int id = rs.getInt("id");
				String login = rs.getString("login");
				String senha = rs.getString("senha");
				String nome = rs.getString("nome");
				String sobrenome = rs.getString("sobrenome");
				String email = rs.getString("email");
				System.out.println("--------------------\nId: " + id + "\nLogin: " + login + "\nSenha: " + senha
						+ "\nNome: " + nome + "\nSobrenome: " + sobrenome + "\nEmail: " + email + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void listarAdministrador() {
		String sql;
		ResultSet rs;
		sql = "Select * from administrador";
		rs = con.buscarSQL(sql);
		try {
			System.out.println("\n###LISTA DE ADMINISTRADORES###\n");
			while (rs.next()) {
				int id = rs.getInt("id");
				String login = rs.getString("login");
				String senha = rs.getString("senha");
				String nome = rs.getString("nome");
				String sobrenome = rs.getString("sobrenome");
				String email = rs.getString("email");
				System.out.println("--------------------\nId: " + id + "\nLogin: " + login + "\nSenha: " + senha
						+ "\nNome: " + nome + "\nSobrenome: " + sobrenome + "\nEmail: " + email + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void listarAluno() {
		String sql = "Select * from aluno";
		ResultSet rs = con.buscarSQL(sql);
		try {
			System.out.println("\n###LISTA DE ALUNOS###\n");
			while (rs.next()) {
				int id = rs.getInt("id");
				String login = rs.getString("login");
				String senha = rs.getString("senha");
				String nome = rs.getString("nome");
				String sobrenome = rs.getString("sobrenome");
				String email = rs.getString("email");
				System.out.println("--------------------\nId: " + id + "\nLogin: " + login + "\nSenha: " + senha
						+ "\nNome: " + nome + "\nSobrenome: " + sobrenome + "\nEmail: " + email + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
