package br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiTUI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConexaoSQL {
	private String url;
	private String usuario;
	private String senha;
	private Connection con;
	public ResultSet rs;
	public Statement stm;

	public ConexaoSQL() {
		url = "jdbc:postgresql://localhost:5432/postgres";
		usuario = "postgres";
		senha = "0904";

		try {
			Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection(url, usuario, senha);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int enviarSQL(String sql) {
		try {
			Statement stm = con.createStatement();
			int res = stm.executeUpdate(sql);
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public ResultSet buscarSQL(String sql) {
		try {
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery(sql);
			return rs;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void executaSQL(String sql) {
		
		try {
			stm = con.createStatement(rs.TYPE_SCROLL_INSENSITIVE,rs.CONCUR_READ_ONLY );
			rs = stm.executeQuery(sql);
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

}