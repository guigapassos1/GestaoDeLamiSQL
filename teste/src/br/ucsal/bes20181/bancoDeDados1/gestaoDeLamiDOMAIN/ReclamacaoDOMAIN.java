package br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiDOMAIN;

public class ReclamacaoDOMAIN {

	private String obsSobrePeca;
	private String pecaComDefeito;
	private int id;
	private int idDaSala;
	private String nomeDoAluno;
	private String status;
	private String nomeDoTecnico;
	
	
	public String getNomeDoTecnico() {
		return nomeDoTecnico;
	}

	public void setNomeDoTecnico(String nomeDoTecnico) {
		this.nomeDoTecnico = nomeDoTecnico;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNomeDoAluno() {
		return nomeDoAluno;
	}

	public void setNomeDoAluno(String nomeDoAluno) {
		this.nomeDoAluno = nomeDoAluno;
	}

	public int getIdDaSala() {
		return idDaSala;
	}

	public void setIdDaSala(int idDaSala) {
		this.idDaSala = idDaSala;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getObsSobrePeca() {
		return obsSobrePeca;
	}

	public void setObsSobrePeca(String obsSobrePeca) {
		this.obsSobrePeca = obsSobrePeca;
	}

	public String getPecaComDefeito() {
		return pecaComDefeito;
	}

	public void setPecaComDefeito(String pecaComDefeito) {
		this.pecaComDefeito = pecaComDefeito;
	}
}
