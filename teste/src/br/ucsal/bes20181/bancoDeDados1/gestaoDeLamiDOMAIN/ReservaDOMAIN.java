package br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiDOMAIN;

public class ReservaDOMAIN {
	
	private int id;
	private String nomeDoProfessor;
	private String nomeDaSala;
	private String dia;
	
	public String getNomeDaSala() {
		return nomeDaSala;
	}
	public void setNomeDaSala(String nomeDaSala) {
		this.nomeDaSala = nomeDaSala;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNomeDoProfessor() {
		return nomeDoProfessor;
	}
	public void setNomeDoProfessor(String nomeDoProfessor) {
		this.nomeDoProfessor = nomeDoProfessor;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	
}