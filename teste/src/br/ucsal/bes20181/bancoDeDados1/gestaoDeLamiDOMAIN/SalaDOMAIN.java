package br.ucsal.bes20181.bancoDeDados1.gestaoDeLamiDOMAIN;

public class SalaDOMAIN {

	private Integer id;
	private String nome;
	private Integer capacidadeDePessoas;
	private Integer quantidadeTotalPcs;
	private Integer pcsFuncionando;
	private String comentario;
	

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String pendencia) {
		this.comentario = pendencia;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getCapacidadeDePessoas() {
		return capacidadeDePessoas;
	}

	public void setCapacidadeDePessoas(Integer capacidadeDePessoas) {
		this.capacidadeDePessoas = capacidadeDePessoas;
	}

	public Integer getQuantidadeTotalPcs() {
		return quantidadeTotalPcs;
	}

	public void setQuantidadeTotalPcs(Integer quantidadeTotalPcs) {
		this.quantidadeTotalPcs = quantidadeTotalPcs;
	}

	public Integer getPcsFuncionando() {
		return pcsFuncionando;
	}

	public void setPcsFuncionando(Integer pcsFuncionando) {
		this.pcsFuncionando = pcsFuncionando;
	}
	

}